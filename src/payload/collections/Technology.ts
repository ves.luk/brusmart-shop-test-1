import type { CollectionConfig } from 'payload/types'

const Technology: CollectionConfig = {
  slug: 'technology',
  admin: {
    useAsTitle: 'title',
  },
  access: {
    read: () => true,
  },
  fields: [
    {
      name: 'title',
      label: 'Název',
      type: 'text',
    }
  ],
}

export default Technology
