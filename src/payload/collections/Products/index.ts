import type {CollectionConfig} from 'payload/types'

import {admins} from '../../access/admins'
import {Archive} from '../../blocks/ArchiveBlock'
import {CallToAction} from '../../blocks/CallToAction'
import {Content} from '../../blocks/Content'
import {MediaBlock} from '../../blocks/MediaBlock'
import {slugField} from '../../fields/slug'
import {populateArchiveBlock} from '../../hooks/populateArchiveBlock'
import {checkUserPurchases} from './access/checkUserPurchases'
import {beforeProductChange} from './hooks/beforeChange'
import {deleteProductFromCarts} from './hooks/deleteProductFromCarts'
import {revalidateProduct} from './hooks/revalidateProduct'
import {ProductSelect} from './ui/ProductSelect'

const materialsArray = [
  {
    label: 'Hliník',
    value: 'hlinik',
  },
  {
    label: 'Nelegovaná ocel',
    value: 'nelegovana_ocel',
  },
  {
    label: 'Nerezová ocel',
    value: 'nerezova_ocel',
  },
  {
    label: 'Neželezné kovy',
    value: 'nezelezne_kovy',
  },
  {
    label: 'Sklo/kámen/keramika',
    value: 'sklo_kamen_keramika',
  },
  {
    label: 'Superslitiny/nátěry',
    value: 'superslitiny_natery',
  },
  {
    label: 'Dřevo',
    value: 'drevo',
  },
  {
    label: 'Kompozitní materiály',
    value: 'kompozitni_materialy',
  },
  {
    label: 'Lak',
    value: 'lak',
  },
]
const machinesArray = [
  {
    label: 'Plošná bruska',
    value: 'plosna_bruska',
  },
  {
    label: 'Robotická bruska',
    value: 'roboticka_bruska',
  },
  {
    label: 'Bezhrotá bruska',
    value: 'bezhrota_bruska',
  },
  {
    label: 'Úhlová bruska',
    value: 'uhlova_bruska',
  },
  {
    label: 'Kontaktní bruska',
    value: 'kontaktni_bruska',
  },
  {
    label: 'Broušení na volném pásu',
    value: 'brouseni_na_volnem_pasu',
  },
  {
    label: 'Dlouhopásová bruska',
    value: 'dlouhopasova_bruska',
  },
  {
    label: 'Širokopásová bruska',
    value: 'sirokopasova_bruska',
  },
  {
    label: 'Planetová bruska',
    value: 'planetova_bruska',
  },
  {
    label: 'Pilníková pásová bruska',
    value: 'pilnikova_pasova_bruska',
  },
  {
    label: 'Excentrická bruska',
    value: 'excentricka_bruska',
  },
  {
    label: 'Podlahová bruska',
    value: 'podlahova_bruska',
  },
  {
    label: 'Talířová bruska',
    value: 'talirova_bruska',
  },
  {
    label: 'Trubková vratná bruska',
    value: 'trubkova_vratna_bruska',
  },

]

const Products: CollectionConfig = {
  slug: 'products',
  admin: {
    useAsTitle: 'title',
    defaultColumns: ['title', 'stripeProductID', '_status'],
    preview: doc => {
      return `${process.env.PAYLOAD_PUBLIC_SERVER_URL}/next/preview?url=${encodeURIComponent(
        `${process.env.PAYLOAD_PUBLIC_SERVER_URL}/products/${doc.slug}`,
      )}&secret=${process.env.PAYLOAD_PUBLIC_DRAFT_SECRET}`
    },
  },
  hooks: {
    beforeChange: [beforeProductChange],
    afterChange: [revalidateProduct],
    afterRead: [populateArchiveBlock],
    afterDelete: [deleteProductFromCarts],
  },
  versions: {
    drafts: true,
  },
  access: {
    read: () => true,
    create: admins,
    update: admins,
    delete: admins,
  },
  fields: [
    {
      name: 'title',
      label: 'Název',
      type: 'text',
      required: true,
    },

    {
      type: 'tabs',
      tabs: [
        {
          label: 'Produkt',
          fields: [
            {
              name: 'short_description',
              label: 'Krátký popisek',
              type: 'textarea',
            },
            {
              name: 'long_description',
              label: 'Celý popisek',
              type: 'textarea',
            },
          ],
        },
        {
          label: 'Detaily produktu',
          fields: [
            {
              name: 'manufacturer',
              label: 'Výrobce',
              type: 'radio',
              options: [
                {
                  label: 'Prázdné',
                  value: 'none',
                },
                {
                  label: 'VSM',
                  value: 'vsm',
                },
                {
                  label: '3M',
                  value: '3m',
                },
                {
                  label: 'Hermes',
                  value: 'hermes',
                },
                {
                  label: 'Smirdex',
                  value: 'smirdex',
                },
              ],
              defaultValue: 'none',
              admin: {
                layout: 'horizontal',
              },
            },
            {
              name: 'for_material_fit',
              label: 'Vyvinuto pro broušení:',
              type: 'select',
              hasMany: true,
              admin: {
                isClearable: true,
                isSortable: false,
              },
              options: materialsArray,
            },
            {
              name: 'for_material_also',
              label: 'Vhodné také pro broušení:',
              type: 'select',
              hasMany: true,
              admin: {
                isClearable: true,
                isSortable: false,
              },
              options: materialsArray,
            },
            {
              name: 'for_machine',
              label: 'Stroje:',
              type: 'select',
              hasMany: true,
              admin: {
                isClearable: true,
                isSortable: false,
              },
              options: machinesArray,
            },
            {
              name: 'more_about_product_description',
              label: 'Více o produktu - popis',
              type: 'richText'
            },
            {
              name: 'benefits',
              type: 'array',
              label: 'Benefity',
              labels: {
                singular: 'Řádek',
                plural: 'Řádky',
              },
              fields: [
                {
                  name: 'text',
                  label: 'Text',
                  type: 'text',
                },
              ],
            },
            {
              name: 'technical_information',
              type: 'array',
              label: 'Technické informace',
              labels: {
                singular: 'Řádek',
                plural: 'Řádky',
              },
              fields: [
                {
                  name: 'name',
                  label: 'Název',
                  type: 'select',
                  hasMany: false,
                  admin: {
                    isClearable: true,
                    isSortable: true,
                  },
                  options: [
                    {label: 'Typ zrna', value: 'typ_zrna'},
                    {label: 'Funkce', value: 'funkce'},
                    {label: 'Pojivo', value: 'pojivo'},
                    {label: 'Barva', value: 'barva'},
                    {label: 'Materiál nosiče', value: 'material_nosice'},
                    {label: 'Flexibilita', value: 'flexibilita'},
                  ],
                },
                {
                  name: 'text',
                  label: 'Text',
                  type: 'text',
                },
              ],
            },
            {
              name: 'files_for_download',
              type: 'array',
              label: 'Soubory ke stažení',
              labels: {
                singular: 'Soubor',
                plural: 'Soubory',
              },
              fields: [
                {
                  name: 'name',
                  label: 'Název souboru',
                  type: 'text',
                },
                {
                  name: 'file',
                  label: 'Soubor',
                  type: 'upload',
                  relationTo: 'media',

                }
              ],
            },
            {
              name: 'grain',
              label: 'Zrno',
              type: 'group',
              fields: [
                {
                  name: 'grain_standard',
                  label: 'Standard zrna',
                  type: 'radio',
                  options: [
                    {
                      label: 'FEPA',
                      value: 'fepa',
                    },
                    {
                      label: 'Trizact',
                      value: 'trizact',
                    },
                  ],
                  defaultValue: 'fepa',
                  admin: {
                    layout: 'horizontal',
                  },
                },
                {
                  name: 'grain_fepa',
                  label: 'Zrno FEPA',
                  type: 'select',
                  hasMany: true,
                  admin: {
                    isClearable: true,
                    isSortable: true,
                    condition: (data, siblingData) => siblingData.grain_standard === 'fepa',
                  },
                  options: [
                    {label: 'P16', value: '16'},
                    {label: 'P20', value: '20'},
                    {label: 'P24', value: '24'},
                    {label: 'P30', value: '30'},
                    {label: 'P36', value: '36'},
                    {label: 'P40', value: '40'},
                    {label: 'P50', value: '50'},
                    {label: 'P60', value: '60'},
                    {label: 'P80', value: '80'},
                    {label: 'P100', value: '100'},
                    {label: 'P120', value: '120'},
                    {label: 'P150', value: '150'},
                    {label: 'P180', value: '180'},
                    {label: 'P220', value: '220'},
                    {label: 'P240', value: '240'},
                    {label: 'P280', value: '280'},
                    {label: 'P320', value: '320'},
                    {label: 'P360', value: '360'},
                    {label: 'P400', value: '400'},
                    {label: 'P500', value: '500'},
                    {label: 'P600', value: '600'},
                    {label: 'P800', value: '800'},
                    {label: 'P1000', value: '1000'},
                    {label: 'P1200', value: '1200'},
                  ],
                },
                {
                  name: 'grain_trizact',
                  label: 'Zrno Trizact',
                  type: 'select',
                  hasMany: true,
                  admin: {
                    isClearable: true,
                    isSortable: true,
                    condition: (data, siblingData) => siblingData.grain_standard === 'trizact',
                  },
                  options: [
                    {label: 'A160', value: '160'},
                    {label: 'A100', value: '100'},
                    {label: 'A80', value: '80'},
                    {label: 'A65', value: '65'},
                    {label: 'A45', value: '45'},
                    {label: 'A30', value: '30'},
                    {label: 'A16', value: '16'},
                    {label: 'A6', value: '6'},
                  ],
                },
              ],
            },
          ],
        },
        {
          label: 'Nastavení objednávky',
          fields: [
            {
              name: 'search_priority',
              label: 'Priorita při vyhledávání (5 = nejvyšší)',
              type: 'number',
              min: 1,
              max: 5,
              defaultValue: 3
            },
            {
              name: 'min_order',
              label: 'Minimální objednávka (ks)',
              type: 'number',
              min: 1,
              defaultValue: 1
            },
            {
              name: 'max_order',
              label: 'Maximální objednávka (ks)',
              type: 'number',
              min: 1,
              defaultValue: 10000
            },
          ],
        },
        {
          label: 'Content',
          fields: [
            {
              name: 'layout',
              type: 'blocks',
              required: true,
              blocks: [CallToAction, Content, MediaBlock, Archive],
            },
          ],
        },
        {
          label: 'Product Details',
          fields: [
            {
              name: 'stripeProductID',
              label: 'Stripe Product',
              type: 'text',
              admin: {
                components: {
                  Field: ProductSelect,
                },
              },
            },
            {
              name: 'priceJSON',
              label: 'Price JSON',
              type: 'textarea',
              admin: {
                readOnly: true,
                hidden: true,
                rows: 10,
              },
            },
            {
              name: 'enablePaywall',
              label: 'Enable Paywall',
              type: 'checkbox',
            },
            {
              name: 'paywall',
              label: 'Paywall',
              type: 'blocks',
              access: {
                read: checkUserPurchases,
              },
              blocks: [CallToAction, Content, MediaBlock, Archive],
            },
          ],
        },
      ],
    },
    /* Sidebar */
    {
      name: 'publishedOn',
      type: 'date',
      admin: {
        position: 'sidebar',
        date: {
          pickerAppearance: 'dayAndTime',
        },
      },
      hooks: {
        beforeChange: [
          ({siblingData, value}) => {
            if (siblingData._status === 'published' && !value) {
              return new Date()
            }
            return value
          },
        ],
      },
    },
    {
      name: 'categories',
      type: 'relationship',
      relationTo: 'categories',
      hasMany: true,
      admin: {
        position: 'sidebar',
      },
    },
    {
      name: 'technology',
      type: 'relationship',
      relationTo: 'technology',
      hasMany: false,
      admin: {
        position: 'sidebar',
      },
    },
    {
      name: 'relatedProducts',
      type: 'relationship',
      relationTo: 'products',
      hasMany: true,
      filterOptions: ({id}) => {
        return {
          id: {
            not_in: [id],
          },
        }
      },
    },
    {
      name:'main_image',
      label:'Náhledový obrázek',
      type:'upload',
      relationTo: 'media',
      admin: {
        position: 'sidebar',
      },
    },
    slugField(),
    {
      name: 'skipSync',
      label: 'Skip Sync',
      type: 'checkbox',
      admin: {
        position: 'sidebar',
        readOnly: true,
        hidden: true,
      },
    },
  ],
}

export default Products
