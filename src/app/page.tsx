import HeroButton from "./_theme/Buttons/HeroButton";
import React from "react";
import BButton from "./_theme/Buttons/BButton";
import SearchBox from "./_theme/Search/SearchBox";
import {draftMode} from "next/headers";
import {fetchDocs} from "./_api/fetchDocs";

export default async function HomepageNew() {
  const { isEnabled: isDraftMode } = draftMode()

  let products;

  try {
    products = await fetchDocs('searchproducts')
  } catch (error) {
    console.error(error) // eslint-disable-line no-console
  }


  return (<>
      <main>
        {/* HERO SECTION */}
        <div className="hero">
          <div className="container container-fluid">
            <div className="grid w-full grid-cols-12">
              <div className="col-span-6 py-14">
                <h1 className={"text-xl uppercase font-medium text-blue-basic"}>Chytré řešení <br/>pro <span className={"after:h-3 after:w-[calc(100%+12px)] after:-left-1.5 after:-z-10 after:bg-red-light after:bottom-2 after:absolute after:mx-auto relative"}>vaše povrchy</span></h1>
                <p className={"mt-8 max-w-xl text-blue-basic leading-6"}>Vyrábíme brusné pásy na míru. Nabízíme široký výběr brusných pásů vhodných pro různé materiály a povrchy, což vám umožní dosáhnout dokonalého výsledku při každém použití. Prozkoumejte naši nabídku a objevte, jak naše produkty mohou efektivnit vaši práci.</p>
              </div>
              <div className="col-span-6">
                <div className="py-14 flex-end-end gap-8 flex-wrap">
                  <HeroButton img={<img className={"w-24 h-full object-contain"} src="/buttonImages/brusne-pasy.jpg" alt=""/>}>
                    <span className={"text-md3"}>Brusné</span><br/>Pásy
                  </HeroButton>
                  <HeroButton img={<img className={"w-24 h-full object-contain"} src="/buttonImages/brusne-kotouce.jpg" alt=""/>}>
                    <span className={"text-md3"}>Brusné</span><br/>Kotouče
                  </HeroButton>
                  <HeroButton img={<img className={"w-24 h-full object-contain"} src="/buttonImages/brusne-role.jpg" alt=""/>}>
                    <span className={"text-md3"}>Brusné</span><br/>Role
                  </HeroButton>
                  <HeroButton img={<img className={"w-24 h-full object-contain"} src="/buttonImages/brusne-archy.jpg" alt=""/>}>
                    <span className={"text-md3"}>Brusné</span><br/>Archy
                  </HeroButton>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* SEARCH BOX SECTION */}
        <SearchBox products={products}/>
        {/* PRODUCTS SHOWREEL BOXES SECTION */}
        <div className={"ShowreelBoxes mt-10"}>
          <div className="container-fluid">
            <h2 className={"text-lg3 text-blue-basic"}>Vyrábíme</h2>
            <div className="mt-10">
              <article className="ShowreelBox grid grid-cols-12 gap-4">
                <div className="col-span-6 p-5">
                  <h1 className={"text-blue-basic font-semibold text-lg4"}>Brusné pásy</h1>
                  <p className={"text-blue-basic leading-6 text-md"}>Vyrábíme brusné pásy na míru. Nabízíme široký výběr brusných pásů vhodných pro různé materiály a povrchy, což vám umožní dosáhnout dokonalého výsledku při každém použití. Prozkoumejte naši nabídku a objevte, jak naše produkty mohou zefektivnit vaši práci.</p>
                  <div className={"mt-4"}>
                    <span className="text-red-light uppercase font-semibold text-md">Oblíbené</span>
                    <div className="flex-start-start gap-2 mt-2 flex-wrap">
                      <BButton buttonType={"link"} link={"/keramika"} >Keramika</BButton>
                      <BButton buttonType={"link"} link={"/keramika"}>Kompaktní zrno</BButton>
                      <BButton buttonType={"link"} link={"/keramika"} >Zirkon-korund</BButton>
                      <BButton buttonType={"link"} link={"/keramika"} >Korund</BButton>
                      <BButton buttonType={"link"} link={"/keramika"} >Netkané rouno</BButton>
                      <BButton buttonType={"link"} link={"/keramika"} >TRIZACT</BButton>
                      <BButton buttonType={"link"} link={"/keramika"} >Leštění</BButton>
                      <BButton buttonType={"link"} link={"/keramika"} >Sic</BButton>
                    </div>
                  </div>
                </div>
                <div className="col-span-6">
                  <img src="/img/showreelbox-pasy.jpg" alt="" className={"w-full object-cover object-center h-full rounded-lg"}/>
                </div>
              </article>
            </div>
          </div>
        </div>
      </main>
    </>
  )
}


