'use client'

import React, { Fragment, useCallback, useEffect, useRef, useState } from 'react'
import qs from 'qs'

import type { Product } from '../../../payload/payload-types'
import type { ArchiveBlockProps } from '../../_blocks/ArchiveBlock/types'
import { Card } from '../Card'
import { Gutter } from '../Gutter'
import { PageRange } from '../PageRange'
import { Pagination } from '../Pagination'

import classes from './index.module.scss'
import Image from "next/image";
import BButton from "../../_theme/Buttons/BButton";
import {renameMaterials} from "../../_theme/Functional/RenameMaterials";

type Result = {
  docs: (Product | string)[]
  hasNextPage: boolean
  hasPrevPage: boolean
  nextPage: number
  page: number
  prevPage: number
  totalDocs: number
  totalPages: number
}

export type Props = {
  categories?: ArchiveBlockProps['categories']
  technology?: ArchiveBlockProps['technology']
  className?: string
  limit?: number
  onResultChange?: (result: Result) => void // eslint-disable-line no-unused-vars
  populateBy?: 'collection' | 'selection'
  populatedDocs?: ArchiveBlockProps['populatedDocs']
  populatedDocsTotal?: ArchiveBlockProps['populatedDocsTotal']
  relationTo?: 'products'
  selectedDocs?: ArchiveBlockProps['selectedDocs']
  showPageRange?: boolean
  sort?: string
}

export const CollectionArchive: React.FC<Props> = props => {
  const {
    categories: catsFromProps,
    technology: technologyFromProps,
    className,
    limit = 10,
    onResultChange,
    populateBy,
    populatedDocs,
    populatedDocsTotal,
    relationTo,
    selectedDocs,
    showPageRange,
    sort = '-createdAt',
  } = props

  const [results, setResults] = useState<Result>({
    docs: (populateBy === 'collection'
      ? populatedDocs
      : populateBy === 'selection'
      ? selectedDocs
      : []
    )?.map(doc => doc.value),
    hasNextPage: false,
    hasPrevPage: false,
    nextPage: 1,
    page: 1,
    prevPage: 1,
    totalDocs: typeof populatedDocsTotal === 'number' ? populatedDocsTotal : 0,
    totalPages: 1,
  })

  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState<string | undefined>(undefined)
  const scrollRef = useRef<HTMLDivElement>(null)
  const hasHydrated = useRef(false)
  const isRequesting = useRef(false)
  const [page, setPage] = useState(1)

  const categories = (catsFromProps || [])
    .map(cat => (typeof cat === 'object' ? cat?.id : cat))
    .join(',')

  const technology = (technologyFromProps || [])
    .map(tech => (typeof tech === 'object' ? tech?.id : tech))
    .join(',')

  const scrollToRef = useCallback(() => {
    const { current } = scrollRef
    if (current) {
      // current.scrollIntoView({
      //   behavior: 'smooth',
      // })
    }
  }, [])

  useEffect(() => {
    if (!isLoading && typeof results.page !== 'undefined') {
      // scrollToRef()
    }
  }, [isLoading, scrollToRef, results])

  useEffect(() => {
    let timer: NodeJS.Timeout = null

    if (populateBy === 'collection' && !isRequesting.current) {
      isRequesting.current = true

      // hydrate the block with fresh content after first render
      // don't show loader unless the request takes longer than x ms
      // and don't show it during initial hydration
      timer = setTimeout(() => {
        if (hasHydrated.current) {
          setIsLoading(true)
        }
      }, 500)

      const searchQuery = qs.stringify(
        {
          depth: 1,
          limit,
          page,
          sort,
          where: {
            ...(categories
              ? {
                  categories: {
                    in: categories,
                  },
                }
              : {}),
            ...(technology
              ? {
                technology: {
                  in: technology,
                },
              }
              : {}),
          },
        },
        { encode: false },
      )

      const makeRequest = async () => {
        try {
          const req = await fetch(
            `${process.env.NEXT_PUBLIC_SERVER_URL}/api/${relationTo}?${searchQuery}`,
          )

          const json = await req.json()
          clearTimeout(timer)

          const { docs } = json as { docs: Product[] }

          if (docs && Array.isArray(docs)) {
            setResults(json)
            setIsLoading(false)
            if (typeof onResultChange === 'function') {
              onResultChange(json)
            }
          }
        } catch (err) {
          console.warn(err) // eslint-disable-line no-console
          setIsLoading(false)
          setError(`Unable to load "${relationTo} archive" data at this time.`)
        }

        isRequesting.current = false
        hasHydrated.current = true
      }

      void makeRequest()
    }

    return () => {
      if (timer) clearTimeout(timer)
    }
  }, [page, categories, technology, relationTo, onResultChange, sort, limit, populateBy])

  return (
    <div className={""}>
      <div className={classes.scrollRef} ref={scrollRef} />
      {!isLoading && error && <Gutter>{error}</Gutter>}
      <Fragment>
        {/*{showPageRange !== false && populateBy !== 'selection' && (
          <Gutter>
            <div className={classes.pageRange}>
              <PageRange
                collection={relationTo}
                currentPage={results.page}
                limit={limit}
                totalDocs={results.totalDocs}
              />
            </div>
          </Gutter>
        )}*/}

        <div className="grid grid-cols-5 grid-test">
          {results.docs?.map((result, index) => {
            if (typeof result === 'object' && result !== null) {
              return (
                <ProductBox doc={result} relationTo={relationTo} showCategories={true}/>
              )
            }

            return null
          })}
        </div>
        {/*<div className="">
          {results.docs?.map((result, index) => {
            if (typeof result === 'object' && result !== null) {
              return (
                <Card doc={result} relationTo={relationTo} showCategories/>
              )
            }

            return null
          })}
        </div>*/}

        {/*{results.totalPages > 1 && populateBy !== 'selection' && (
            <Pagination
              className={classes.pagination}
              onClick={setPage}
              page={results.page}
              totalPages={results.totalPages}
            />
          )}*/}

      </Fragment>
    </div>
  )
}

export function ProductBox(props: any) {
  const href = `/products/${props.doc.slug}`
  const hasCategories = props.categories && Array.isArray(props.categories) && props.categories.length > 0

  return (
    <article className="bg-white grid__item">
      <div className="py-6 px-6 relative">
        <div className="absolute right-6 top-6 text-[11px] bg-blue-basic rounded-md uppercase py-0.5 px-2 text-white font-semibold">Nejlepší nabídka</div>
        <div className="flex-center">
          <Image alt={""} src={"/img/product-AK890Y-big.jpg"} width={290} height={290} className={"w-10/12"}/>
        </div>
        <h1 className={"text-blue-basic font-bold text-lg mt-3"}>{props.doc.title}</h1>
        <p className={"text-sm mt-2"}>{props.doc.short_description}</p>
        {props.doc?.for_material_fit &&
        <div className={"mt-2"}>
          <h2 className="text-green-500 font-semibold text-xsm">Nejlepší k broušení:</h2>
          <div className={"text-sm w-full divide-x divide-gray-200"}>
            {renameMaterials(props.doc?.for_material_fit).map(material => (
              <span className={"inline-block px-1"}>{material.label}</span>
            ))}
          </div>
        </div>
        }
        <BButton buttonType={"normal"} size={"big"} link={href} className={"w-full text-center mt-3"}>Zobrazit detail</BButton>
        <div>
          {props.showCategories && hasCategories && (
            <div className={classes.leader}>
              {props.showCategories && hasCategories && (
                <div>
                  {props.categories?.map((category, index) => {
                    if (typeof category === 'object' && category !== null) {
                      const { title: titleFromCategory } = category

                      const categoryTitle = titleFromCategory || 'Untitled category'

                      const isLast = index === props.categories.length - 1

                      return (
                        <Fragment key={index}>
                          {categoryTitle}
                          {!isLast && <Fragment>, &nbsp;</Fragment>}
                        </Fragment>
                      )
                    }

                    return null
                  })}
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    </article>
  )
}
