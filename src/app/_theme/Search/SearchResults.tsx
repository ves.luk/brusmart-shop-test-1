import SearchResultItem from "./components/SearchResultItem";
import React from "react";

export default function SearchResults(props) {
  return (
    <div className="SearchResults divide-gray-400 divide-y">
      {props.products
        .sort((a, b) => b.search_priority - a.search_priority)
        .map((product) => (
          product._status === "published" &&
          <SearchResultItem key={product.id} product={product} />
        ))}
{/*
      <SearchResultItem/>
      <SearchResultItem/>*/}
    </div>
  )
}
