'use client'

import React, { Fragment, useCallback, useEffect, useRef, useState } from 'react'
import qs from 'qs'
import MagnifierSvg from "../../../../public/magnifier.svg";
import BButton from "../Buttons/BButton";
import {Gutter} from "../../_components/Gutter";
import {Card} from "../../_components/Card";
import {Product} from "../../../payload/payload-types";
import {ArchiveBlockProps} from "../../_blocks/ArchiveBlock/types";
import SearchResults from "./SearchResults";

type Result = {
  docs: (Product | string)[]
  hasNextPage: boolean
  hasPrevPage: boolean
  nextPage: number
  page: number
  prevPage: number
  totalDocs: number
  totalPages: number
}

export type Props = {
  categories?: ArchiveBlockProps['categories']
  className?: string
  limit?: number
  onResultChange?: (result: Result) => void // eslint-disable-line no-unused-vars
  populateBy?: 'collection' | 'selection'
  populatedDocs?: ArchiveBlockProps['populatedDocs']
  populatedDocsTotal?: ArchiveBlockProps['populatedDocsTotal']
  relationTo?: 'products'
  selectedDocs?: ArchiveBlockProps['selectedDocs']
  showPageRange?: boolean
  sort?: string
}


export default function SearchBox(props) {


  const [searchTerm, setSearchTerm] = useState('');
  const [selectedCategories, setSelectedCategories] = useState([]);
  const [selectedSandingMaterials, setSelectedSandingMaterials] = useState([]);
  const [searchResultsVisible, setSearchResultsVisible] = useState(false);

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleCategoryChange = (event) => {
    const category = event.target.value;
    setSelectedCategories((prevCategories) =>
      prevCategories.includes(category)
        ? prevCategories.filter((cat) => cat !== category)
        : [...prevCategories, category]
    );
  };

  const handleSandingMaterialsChange = (event) => {
    const sandingMaterials = event.target.value;
    setSelectedSandingMaterials((prevSandingMaterials) =>
      prevSandingMaterials.includes(sandingMaterials)
        ? prevSandingMaterials.filter((sandMat) => sandMat !== sandingMaterials)
        : [...prevSandingMaterials, sandingMaterials]
    );
  };



  const filteredProducts = props.products.filter((product) => {
    const matchesSearchTerm = product.title.toLowerCase().includes(searchTerm.toLowerCase());
    const productCategories = product.categories.map(cat => cat.title);
    const productSandingMaterials = product.for_material_fit.map(sandMat => sandMat);
    const matchesCategory = selectedCategories.length === 0 || selectedCategories.some(category => productCategories.includes(category));
    const matchesSandingMaterials = selectedSandingMaterials.length === 0 || selectedSandingMaterials.some(sandingMaterial => productSandingMaterials.includes(sandingMaterial));
    return matchesSearchTerm && matchesCategory && matchesSandingMaterials;
  });


  useEffect(() => {
    if (
      searchTerm === '' &&
      selectedCategories.length === 0 &&
      selectedSandingMaterials.length === 0
    ) {
      setSearchResultsVisible(false);
    } else {
      setSearchResultsVisible(true);
    }
  }, [searchTerm, selectedCategories, selectedSandingMaterials]);

  return (
    <div>
      <div className="container-fluid test-searchbox-dim">
        <div className="relative">
          <div className="SearchBox">
            <div className="py-7 px-8 rounded-lg border border-black w-full bg-white">
              <div className="SearchEngine flex-start-start">
                <div className="flex-start-start flex-wrap pr-8 border-r border-gray-500">
                  <h2 className={"text-lg3 text-blue-basic"}>Vyhledávání produktu</h2>
                  <div className="flex-start-center border-b border-gray-500 pb-1 mt-4">
                    <MagnifierSvg className={"mr-4 [&>path]:stroke-green-500 w-6"}/>
                    <input value={searchTerm}
                           onChange={handleSearch}
                           type="search"
                           placeholder={"zadejte sérii / výrobce / název"}
                           className={"min-w-72 placeholder:text-green-500 placeholder:font-medium text-md text-blue-basic font-bold"}/>
                  </div>
                </div>
                <div className="flex-start-start pl-8 gap-14">
                  <div className="flex-start-start flex-wrap flex-col">
                    <h3 className={"font-semibold text-blue-basic text-md2"}>Filtrovat produkty</h3>
                    <div className="FilterCategory flex flex-wrap flex-col mt-2">
                      <h4 className={"font-semibold text-green-500 text-md"}>Kategorie</h4>
                      <div className="flex-start-start gap-2 mt-2">
                        <BButton buttonType={"check"} idName={"pasy"} value="Brusné pásy"
                                 checked={selectedCategories.includes('Brusné pásy')}
                                 onChange={handleCategoryChange}>Pásy</BButton>
                        <BButton buttonType={"check"} idName={"kotouce"} value="Brusný kotouč"
                                 checked={selectedCategories.includes('Brusný kotouč')}
                                 onChange={handleCategoryChange}>Kotouče</BButton>
                        <BButton buttonType={"check"} idName={"role"}>Role</BButton>
                        <BButton buttonType={"check"} idName={"archy"}>Archy</BButton>
                      </div>
                    </div>
                  </div>

                  <div className="FilterCategory flex flex-wrap">
                    <h4 className={"font-semibold text-green-500 text-md"}>Jaký materiál brousíte?</h4>
                    <div className="flex-start-start flex-wrap gap-2 mt-2">
                      <BButton buttonType={"check"} size={"small"} idName={"nerezova-ocel"} value="Nerezová ocel"
                               checked={selectedSandingMaterials.includes('Nerezová ocel')}
                               onChange={handleSandingMaterialsChange}>nerezová ocel</BButton>
                      <BButton buttonType={"check"} size={"small"} idName={"hlinik"} value="hlinik"
                               checked={selectedSandingMaterials.includes('hlinik')}
                               onChange={handleSandingMaterialsChange}>hliník</BButton>
                      <BButton buttonType={"check"} size={"small"} idName={"superslitiny"} value="superslitiny"
                               checked={selectedSandingMaterials.includes('superslitiny')}
                               onChange={handleSandingMaterialsChange}>superslitiny</BButton>
                      <BButton buttonType={"check"} size={"small"} idName={"ocel"}>ocel</BButton>
                      <BButton buttonType={"check"} size={"small"} idName={"inox"}>inox</BButton>
                      <BButton buttonType={"check"} size={"small"} idName={"komposit"}>komposit</BButton>
                      <BButton buttonType={"check"} size={"small"} idName={"drevo"}>dřevo</BButton>
                      <BButton buttonType={"check"} size={"small"} idName={"kovy"}>kovy</BButton>
                      <BButton buttonType={"check"} size={"small"} idName={"sklo-kamen-keramika"}>sklo/kámen/keramika</BButton>

                      <BButton buttonType={"check"} size={"small"} idName={"laky"}>laky</BButton>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {searchResultsVisible &&
          <div className="absolute bottom-0 border border-black border-t-0 rounded-bl-xl rounded-br-xl bg-white w-[820px] translate-y-full left-10 z-10 shadow-2xl overflow-hidden">
            <SearchResults products={filteredProducts}/>
          </div>
          }
        </div>
      </div>
      <div className="test-searchbox-overlay"></div>


    </div>
  )
}
