import Image from "next/image";
import BButton from "../../Buttons/BButton";
import React from "react";
import {renameMaterials} from "../../Functional/RenameMaterials";
import MaterialButton from "../../Buttons/MaterialButton";
import Link from "next/link";

export default function SearchResultItem(props: any) {
  const grainArray = props.product.grain.grain_fepa.map(grain => parseInt(grain.replace(/_/g, ''), 10));
  const smallestGrain = Math.min(...grainArray);
  const largestGrain = Math.max(...grainArray);

  return (
    <article className="SearchResultItem py-6 px-10 hover:bg-gray-100 group relative">
      <Link href={"products/" + props.product.slug} className={"!no-underline"}>
        <div className="grid grid-cols-12">
          <div className="col-span-4 flex-start-end gap-5">
            <Image alt={""} src={"/img/product-AK890Y.jpg"} width={50} height={50}/>
            <h1 className={"text-lg text-blue-basic font-bold leading-6 -mt-2"}><span className={"text-red-light font-light text-md"}>{props.product.categories[0].title}</span> <br/>{props.product.title}</h1>
          </div>
          <div className="col-span-8 flex-between-end">
            <div className="flex-start-start gap-8">
              <div>
                <h2 className="text-green-500 font-semibold text-xsm">Nejlepší k broušení:</h2>
                <div className={"text-sm w-full divide-x divide-gray-200"}>
                  {renameMaterials(props.product.for_material_fit).map((material, index, array) => (
                    <span className={`inline-block ${index === 0 ? 'pr-2' : 'pl-2 pr-2'}`}>{material.label}</span>
                  ))}
                </div>
              </div>
              <div>
                <h2 className="text-green-500 font-semibold text-xsm">Rozsah zrna:</h2>
                <div className={"text-sm w-full flex-start-center gap-1"}>
                  <div className={"flex-start-center gap-2"}>
                    <div className={"text-[11px] font-semibold h-5 px-1 bg-gray-300 text-blue-basic flex-center"}>{smallestGrain}</div>
                    <div className="w-2.5 h-0.5 bg-blue-basic"/>
                    <div className={"text-[11px] font-semibold h-5 px-1 bg-gray-300 text-blue-basic flex-center"}>{largestGrain}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={"mt-4"}>
          <p className={"text-sm"}>{props.product.short_description}</p>
        </div>
      </Link>
      <div className="group-hover:opacity-100 opacity-0 transition-all absolute right-5 bottom-5 z-10">
        <BButton buttonType={"normal"} link={"products/" + props.product.slug}>Zobrazit</BButton>
      </div>
    </article>
  )
}
