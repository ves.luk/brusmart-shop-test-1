import LogoSvg from "../../../../public/logo-basic.svg";
import MagnifierSvg from "../../../../public/magnifier.svg";
import BasketSvg from "../../../../public/basket.svg";
import PersonSvg from "../../../../public/person.svg";
import React from "react";
import Link from "next/link";
import {AdminBar} from "../../_components/AdminBar";

export default function Header() {
  return (
    <header>
      <div className="border-b border-black py-2.5 flex-start-center">
        <div className="container ml-0">
          <nav>
            <ul className={"flex gap-10 text-sm"}>
              <li><Link href="#" className={"no-underline"}>Firma</Link></li>
              <li><Link href="#" className={"no-underline"}>Sortiment</Link></li>
              <li><Link href="#" className={"no-underline"}>Ke stažení</Link></li>
              <li><Link href="#" className={"no-underline"}>Kontakt</Link></li>
            </ul>
          </nav>
        </div>
      </div>
      <div className={"border-b border-black flex-start-center h-[64px]"}>
        <div className="container flex-between-center">
          <Link href="/" className={"py-5 inline-block"}><LogoSvg/></Link>
          <div className="">
            <AdminBar/>
          </div>
        </div>
        <div className="flex-start-center h-full">
          <div className="border-l border-black">
            <div className="px-5 py-4 flex-start-center">
              <MagnifierSvg className={"mr-4"}/>
              <input type="search" placeholder={"Vyhledávání produktu..."} className={"min-w-72"}/>
            </div>
          </div>
          <a href="#" className={"aspect-square bg-white h-full flex-center border-l border-black"}><BasketSvg/></a>
          <a href="#" className={"aspect-square bg-black h-full flex-center"}><PersonSvg className={"[&>path]:fill-white"}/></a>
        </div>
      </div>
      <div className={"border-b border-black h-[60px] flex-start-center"}>
        <div className="container ml-0 flex-start-center">
          <nav>
            <ul className={"flex-start-center gap-x-12"}>
              <li className={"bullet-divider"}><Link href="/brusne-pasy" className={"no-underline !decoration-red-light decoration-2"}>Brusné pásy</Link></li>
              <li className={"bullet-divider"}><Link href="/brusne-kotouce" className={"no-underline !decoration-red-light decoration-2"}>Brusné kotouče</Link></li>
              <li className={"bullet-divider"}><Link href="/brusne-archy" className={"no-underline !decoration-red-light decoration-2"}>Brusné archy</Link></li>
              <li className={"bullet-divider"}><Link href="/brusne-role" className={"no-underline !decoration-red-light decoration-2"}>Brusné role</Link></li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  )
}
