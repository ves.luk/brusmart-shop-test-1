import React from "react";
import Link from "next/link";

export default function ActionButton({children, link, className, icon, appearance}: { children: React.ReactNode, link: string,className?: string, icon?:React.ReactNode, appearance?:string }) {
  const buttonStyle = `${icon ? "flex-between-center gap-3" : "flex-center"} ${appearance === "primary" ? "bg-blue-basic text-white hover:bg-red-basic" : "bg-white text-blue-basic hover:bg-gray-300"} text-center uppercase py-4 px-5 min-w-xs select-none cursor-pointer rounded-lg transition-colors duration-200 ease-in-out border border-gray-400 font-semibold text-md !no-underline ${className}`;
  return (
    <div className="ActionButton inline-block">
      <Link href={link} className={buttonStyle + (appearance === "primary" ? " [&>svg>path]:fill-white" : " ")}><span className={"flex-1"}>{children}</span>{icon}</Link>
    </div>
  )
}
