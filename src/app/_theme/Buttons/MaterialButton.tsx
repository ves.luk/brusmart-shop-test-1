import React from "react";
import Link from "next/link";

export default function MaterialButton({children, link, className, icon}: { children: React.ReactNode, link: string,className?: string, icon?:React.ReactNode }) {
  const buttonStyle = `[&>img]:w-9 flex-between-center gap-3 select-none cursor-pointer rounded-lg transition-colors duration-200 ease-in-out border border-gray-400 font-normal text-blue-basic hover:bg-gray-300 text-md overflow-hidden !no-underline ${className}`;
  return (
    <div className="MaterialButton inline-block">
      <Link href={link} className={buttonStyle}>{icon}{children}</Link>
    </div>
  )
}
