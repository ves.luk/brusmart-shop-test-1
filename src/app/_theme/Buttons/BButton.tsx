import React from "react";
import Link from "next/link";

export default function BButton({children, idName, size, link, buttonType, className, value, onChange, checked}: { children: React.ReactNode, idName?: string, size?: string, link?: string, buttonType: string, className?: string, value?: any, onChange?: any, checked?: any }) {
  const buttonSize = !size ? "normal" : size;
  const buttonStyleCheck = `border border-gray-400 font-medium text-blue-basic peer-checked:bg-blue-basic peer-checked:text-white peer-checked:border-blue-basic hover:bg-gray-300 ${buttonSize === "small" ? "py-0.5 px-2 text-base" : buttonSize === "normal" ? "py-1 px-3 text-md" : ""}`
  const buttonStyleLink = `border border-gray-400 font-medium text-blue-basic hover:bg-gray-300 !no-underline ${buttonSize === "small" ? "py-0.5 px-2 text-base" : buttonSize === "normal" ? "py-1 px-3 text-md" : ""}`;
  const buttonStyleNormal = `uppercase hover:bg-gray-100 bg-white text-red-light text-sm font-bold border border-gray-400 !no-underline ${buttonSize === "small" ? "py-0.5 px-2 text-xsm" : buttonSize === "normal" ? "py-1 px-3 text-sm" : buttonSize === "big" && "py-3 px-5 text-md"}`;
  const buttonStyle = `inline-block select-none cursor-pointer rounded-lg transition-colors duration-200 ease-in-out ${buttonType === "check" ? buttonStyleCheck : buttonType === "link" ? buttonStyleLink : buttonType === "normal" && buttonStyleNormal} ${className}`;
  return (
    <div className="BButton">
      {buttonType === "check" ?
        <>
          <input type="checkbox"
                 id={idName}
                 className="peer hidden"
                 checked={checked}
                 onChange={onChange}
                 value={value}/>
          <label htmlFor={idName} className={buttonStyle}>{children}</label>
        </> : buttonType === "link" ?
          <Link href={link} className={buttonStyle}>{children}</Link>
          : buttonType === "normal" &&
          <Link href={link} className={buttonStyle}>{children}</Link>
      }
    </div>
  )
}
