import React from "react";

export default function HeroButton({children, img}: { children: React.ReactNode, img?: React.ReactNode }) {
  return (
    <>
      <button className={"rounded-lg border border-black py-3 px-3 flex-start-center text-lg4 h-[120px] bg-white hover:bg-gray-100"}>
        {img}
        <div className="text-left leading-8 font-light ml-6 mr-8 -mt-2">{children}</div>
      </button>
    </>
  )
}
