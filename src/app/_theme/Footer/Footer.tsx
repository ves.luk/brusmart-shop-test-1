import Link from "next/link";
import LogoSvg from "../../../../public/logo-basic.svg";
import React from "react";

export default function Footer() {
  return (
    <footer className={"mt-20"}>
      <div className="w-full h-px origin-top-left bg-gradient-to-r from-white via-gray-400 to-white"/>
      <div className="container-fluid bg-gray-100 py-20">
        <div className="grid grid-cols-12 gap-10">
          <div className="col-span-4">
            <Link href={"/"}><LogoSvg/></Link>
            <span className={"mt-12 inline-block"}><b>BRUSMART CZ s. r. o.</b><br/>
                <span className={"font-semibold text-green-500 text-md pt-2 inline-block"}>Registrační adresa:</span><br/>
                Zahrádkářská 389/38, Svobodné Dvory<br/>
                503 11 HRADEC KRÁLOVÉ<br/>
                <span className="pt-2 inline-block">
                  <span className={"font-semibold text-green-500 text-md"}>IČ:</span> 09759069<br/>
                  <span className={"font-semibold text-green-500 text-md"}>DIČ:</span> CZ09759069<br/>
                </span>
                <Link href={"https://or.justice.cz/ias/ui/rejstrik-firma.vysledky?subjektId=1104360&typ=PLATNY"} target={"_blank"} className={"inline-block w-full pt-2"}>Obchodní rejstřík</Link>

              </span>
          </div>
          <div className="col-span-8 flex-center-start gap-28">
            <div className="">
              <span className="text-md3 font-medium text-blue-basic">Firma</span>
              <ul className={"mt-12 space-y-2"}>
                <li>
                  <Link href={"#"} className={"no-underline"}>Firma</Link>
                </li>
                <li>
                  <Link href={"#"} className={"no-underline"}>Sortiment</Link>
                </li>
                <li>
                  <Link href={"#"} className={"no-underline"}>Ke stažení</Link>
                </li>
                <li>
                  <Link href={"#"} className={"no-underline"}>Kontakt</Link>
                </li>
              </ul>
            </div>
            <div className="">
              <span className="text-md3 font-medium text-blue-basic">Sortiment</span>
              <ul className={"mt-12 space-y-2"}>
                <li>
                  <Link href={"#"} className={"no-underline"}>Brusné pásy</Link>
                </li>
                <li>
                  <Link href={"#"} className={"no-underline"}>Brusné kotouče</Link>
                </li>
                <li>
                  <Link href={"#"} className={"no-underline"}>Brusné role</Link>
                </li>
                <li>
                  <Link href={"#"} className={"no-underline"}>Brusné archy</Link>
                </li>
              </ul>
            </div>
            <div className="">
              <span className="text-md3 font-medium text-blue-basic w-full inline-block">Výroba</span>
              <span className={"mt-12 inline-block"}>
                  Pod Hroby 130, 280 02 KOLÍN – Kolín IV <br/>
                  Telefon: <Link href={"tel:+420774230008"}> +420 774 230 008</Link> <br/>
                  E-mail: <Link href={"mailto:info@brusmart.cz"}>info@brusmart.cz</Link> <br/>
                  Web: <Link href={"https://brusmart.cz"}>brusmart.cz</Link> <br/>
                </span>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}
