export function renameMachine(machines) {
  const machineLabels = {
    bezhrota_bruska: "Bezhrotá bruska",
    plosna_bruska: "Plošná bruska"
  };

  return machines.map(machine => ({
    name: machine,
    label: machineLabels[machine] || machine
  }));
}
