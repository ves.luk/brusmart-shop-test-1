export function renameTechnicalInformation(technicalInformation) {
  const infoLabels = {
    typ_zrna: "Typ zrna",
    barva: "Barva",
    funkce: "Funkce",
    material_nosice: "Materiál nosiče",
    pojivo: "Pojivo",
    flexibilita: "Flexibilita"
  };

  return technicalInformation.map(entry => ({
    ...entry,
    label: infoLabels[entry.name] || entry.name
  }));
}
