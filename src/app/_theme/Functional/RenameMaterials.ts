export function renameMaterials(materials) {
  const materialLabels = {
    hlinik: "Hliník",
    nelegovana_ocel: "Nelegovaná ocel",
    nerezova_ocel: "Nerezová ocel",
    nezelezne_kovy: "Neželezné kovy",
    sklo_kamen_keramika: "Sklo/kámen/keramika",
    superslitiny_natery: "Superslitiny/nátěry",
    drevo: "Dřevo",
    kompozitni_materialy: "Kompozitní materiály",
    lak: "Lak"
  };

  return materials?.map(material => ({
    name: material,
    label: materialLabels[material] || material // Fallback to the original name if no label is found
  }));
}
