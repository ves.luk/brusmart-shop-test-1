import React from 'react'

import {CollectionArchive} from '../../_components/CollectionArchive'
import {Gutter} from '../../_components/Gutter'
import RichText from '../../_components/RichText'
import {ArchiveBlockProps} from './types'

import classes from './index.module.scss'

export const ArchiveBlock: React.FC<
  ArchiveBlockProps & {
  id?: string
}
> = props => {
  const {
    introContent,
    id,
    relationTo,
    populateBy,
    limit,
    populatedDocs,
    populatedDocsTotal,
    selectedDocs,
    categories,
    technology
  } = props

  return (
    <div id={`block-${id}`} className="Technology w-full">
      <div className="w-full bg-gradient-to-t from-gray-100/50 to-white py-3 border-b border-black">
        <div className="container-fluid flex-between-center">
          <h1 className="text-lg3 font-medium text-blue-basic flex-1">{technology[0].title}</h1>
          {introContent && (
            <div className="">
              <RichText content={introContent}/>
            </div>
          )}
        </div>
      </div>
      <CollectionArchive
        populateBy={populateBy}
        relationTo={relationTo}
        populatedDocs={populatedDocs}
        populatedDocsTotal={populatedDocsTotal}
        selectedDocs={selectedDocs}
        categories={categories}
        technology={technology}
        limit={limit}
        sort="-publishedOn"
      />
    </div>
  )
}
