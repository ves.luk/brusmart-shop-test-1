import { ARCHIVE_BLOCK, CALL_TO_ACTION, CONTENT, MEDIA_BLOCK } from './blocks'
import { CATEGORIES } from './categories'
import { META } from './meta'
import {TECHNOLOGY} from "./technology";

export const PRODUCTS = `
  query Products {
    Products(limit: 300) {
      docs {
        slug
      }
    }
  }
`

export const SEARCH_PRODUCTS = `
  query SearchProducts {
    Products(limit: 300) {
      docs {
        id
        _status
        title
        slug
        categories {
          id
          title
        }
        for_material_fit
        for_material_also
        search_priority
        grain {
          grain_fepa
        }
        main_image {
          url
          width
          height
        }
        short_description
      }
    }
  }
`

export const PRODUCT = `
  query Product($slug: String, $draft: Boolean) {
    Products(where: { slug: { equals: $slug}}, limit: 1, draft: $draft) {
      docs {
        id
        title
        stripeProductID
        ${CATEGORIES}
        ${TECHNOLOGY}
        layout {
          ${CALL_TO_ACTION}
          ${CONTENT}
          ${MEDIA_BLOCK}
          ${ARCHIVE_BLOCK}
        }
        priceJSON
        enablePaywall
        relatedProducts {
          id
          slug
          title
          ${META}
        }
        short_description
        long_description
        manufacturer
        technology {
          id
          title
        }
        for_material_fit
        for_material_also
        for_machine
        more_about_product_description
        benefits {
          text
        }
        technical_information {
          name
          text
        }
        files_for_download {
          name
          file {
            url
          }
        }
        grain {
          grain_fepa
        }
        main_image {
          url
          width
          height
        }
        ${META}
      }
    }
  }
`


export const PRODUCT_PAYWALL = `
  query Product($slug: String, $draft: Boolean) {
    Products(where: { slug: { equals: $slug}}, limit: 1, draft: $draft) {
      docs {
        paywall {
          ${CALL_TO_ACTION}
          ${CONTENT}
          ${MEDIA_BLOCK}
          ${ARCHIVE_BLOCK}
        }
      }
    }
  }
`
