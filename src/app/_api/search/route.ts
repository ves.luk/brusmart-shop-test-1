// app/api/search/route.js


import payload from "payload";

export async function GET(request) {
  const { searchParams } = new URL(request.url);
  const title = searchParams.get('title') || '';

  const result = await payload.find({
    collection: 'pages',
    depth: 2,
    page: 1,
    limit: 10,
    pagination: false,
    where: {
      title: {
        equals: title,
      },
    },
    sort: '-title',
    locale: 'en',
    fallbackLocale: false,
    user: null, // replace with your actual user if needed
    overrideAccess: false,
    showHiddenFields: true,
  });

  return new Response(JSON.stringify(result), {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}
