import Header from "../../_theme/Header/Header";
import Footer from "../../_theme/Footer/Footer";
import Image from "next/image";
import MaterialButton from "../../_theme/Buttons/MaterialButton";
import React from "react";
import Link from "next/link";
import PdfIcon from "../../../../public/icons/pdf.svg";
import ActionButton from "../../_theme/Buttons/ActionButton";
import DoubleChevronIcon from "../../../../public/icons/double-chevron.svg";

export default function ProductDetail() {
  return (
    <>
      <Header/>
      <article>
        <div className="container-fluid">
          <div className="grid grid-cols-12 gap-10">
            <div className="col-span-7">
              {/* --------- MAIN CONTENT*/}

              {/* HERO */}
              <div className="flex-between-start mt-8 gap-10">
                <div className={"flex-1"}>
                  <h1 className={"text-xl text-blue-basic font-light leading-[3rem]"}><span className={"text-red-light text-lg2"}>Brusný pás</span><br/>AK890Y</h1>
                  <p className={"text-base mt-4"}>ACTIROX s geometricky tvarovaným keramickým brusným zrnem pro maximální úběr materiálu. Agresivní brus kombinovaný se slabým přítlakem přináší nižší hlučnost a vibrace, a tudíž menší namáhání stroje a jeho obsluhy. Rychlejší broušení výrazně zkracuje dobu obrábění. Vrstva TOP SIZE umožňuje chladný brus, takže se výrazně redukuje odpad a není nutné další obrábění.</p>
                </div>
                <div className="flex-center h-full">
                  <Image alt={""} src={"/img/product-AK890Y-big.jpg"} width={230} height={230} className={"mt-6 object-contain"}/>
                </div>
              </div>
              {/* HERO INFO - MATERIALS */}
              <div className="space-y-2.5 mt-5">
                <div>
                  <span className="font-semibold text-green-500 text-sm">Technologie zrna:</span>
                  <p className={"font-semibold text-md2 uppercase"}>VSM CERAMICS</p>
                </div>
                <div>
                  <span className="font-semibold text-green-500 text-sm">Vyvinuto pro:</span>
                  <ul className={"flex-start-start flex-wrap gap-3 mt-1"}>
                    <li><MaterialButton link={"#"} icon={<img src="/materials/nelegovana-ocel.jpg" alt=""/>}>Nelegovaná ocel</MaterialButton></li>
                    <li><MaterialButton link={"#"} icon={<img src="/materials/nerezova-ocel.jpg" alt=""/>}>Nerezová ocel</MaterialButton></li>
                    <li><MaterialButton link={"#"} icon={<img src="/materials/superslitiny.jpg" alt=""/>}>Superslitiny</MaterialButton></li>
                  </ul>
                </div>
                <div>
                  <span className="font-semibold text-green-500 text-sm">Vhodné také pro:</span>
                  <ul className={"flex-start-start flex-wrap gap-3 mt-1"}>
                    <li><MaterialButton link={"#"} icon={<img src="/materials/nelegovana-ocel.jpg" alt=""/>}>Nelegovaná ocel</MaterialButton></li>
                    <li><MaterialButton link={"#"} icon={<img src="/materials/nerezova-ocel.jpg" alt=""/>}>Nerezová ocel</MaterialButton></li>
                    <li><MaterialButton link={"#"} icon={<img src="/materials/superslitiny.jpg" alt=""/>}>Superslitiny</MaterialButton></li>
                  </ul>
                </div>
              </div>

              {/* DIVIDER */}
              <div className="w-full h-px origin-top-left bg-gradient-to-r from-white via-gray-400 to-white my-6"/>

              {/* BENEFITS */}
              <div>
                <h2 className={"text-lg2 font-light"}>Benefity AK890Y</h2>
                <p className={"mt-2"}>
                  <ul className={"text-base font-normal space-y-1.5 benefits-list"}>
                    <li>Maximální úběr materiálu díky geometricky tvarovanému keramickému zrnu přináší snížení provozních</li>
                    <li>Špičkový výkon se superslitinami, nerezovou ocelí a nelegovanou ocelí díky optimalizované hustotě povlaku</li>
                  </ul>
                </p>
              </div>

              {/* DIVIDER */}
              <div className="w-full h-px origin-top-left bg-gradient-to-r from-white via-gray-400 to-white my-6"/>

              {/* MORE ABOUT PRODUCT */}
              <div>
                <h2 className={"text-lg2 font-light"}>Více o produktu</h2>
                <p className={"mt-2 text-base"}>Plus v brusném výkonu
                  Praxe ukázala, že brusiva ACTIROX mají vyšší brusný výkon než srovnatelná konvenční brusiva – s konzistentním výsledným povrchem. S konkrétní zrnitostí dosáhnete většího úběru, než byste očekávali, ale povrch není drsnější. Skutečné plus pro váš proces broušení.</p>
              </div>

              {/* TECHNICAL INFO - TABLE */}
              <div className={"mt-8"}>
                <h3 className={"text-md3 font-light"}>Technické údaje</h3>
                <ul className={"divide-y divide-green-300 border-y border-green-300 mt-2 max-w-sm"}>
                  <li className={"flex-between-center py-2 px-3 flex-wrap"}><span className={"text-green-500 font-medium text-base"}>Typ zrna:</span> <span className={"text-base"}>Keramika</span></li>
                  <li className={"flex-between-center py-2 px-3 flex-wrap"}><span className={"text-green-500 font-medium text-base"}>Typ zrna:</span> <span className={"text-base"}>Keramika</span></li>
                  <li className={"flex-between-center py-2 px-3 flex-wrap"}><span className={"text-green-500 font-medium text-base"}>Typ zrna:</span> <span className={"text-base"}>Keramika</span></li>
                  <li className={"flex-between-center py-2 px-3 flex-wrap"}><span className={"text-green-500 font-medium text-base"}>Vhodné pro stroje:</span> <span className={"text-base"}>Bezhrotá bruska, širokopásová bruska, planetová bruska,  plošná bruska, kontaktní bruska, robotická bruska, broušení na volném pásu</span></li>
                </ul>
              </div>

              {/* DOWNLOAD FILES - INFORMATION MATERIALS */}
              <div className={"mt-8"}>
                <h3 className={"text-md3 font-light"}>Informační material</h3>
                <div className={"space-y-1 mt-2"}>
                  <Link href={"#"} download={true} className={"flex-start-center gap-2"}>
                    <div className="bg-gray-100 rounded-lg py-1 px-1 inline-block"><PdfIcon className={"[&>path]:fill-red-light w-5 h-5"}/></div>
                    Přehled údajů o výrobku (v angličtině)</Link>
                </div>
              </div>
            </div>
            <div className="col-span-5">
              <div className="border border-gray-400 rounded-lg mt-12 sticky top-12">
                <div className="flex-between-center py-3 px-5">
                  <span className={"text-md3"}>Přidat do poptávky</span>
                  <span className={"text-green-500"}>Uloženo</span>
                </div>
                <div>
                  <table className={"w-full"}>
                    <thead>
                    <tr>
                      <th className={"text-center bg-gray-100 py-2 px-5"}>#</th>
                      <th className={"text-left bg-gray-100 py-2 pr-4"}>
                        <span className={"uppercase font-bold block text-sm"}>Šířka</span>
                        <span className={"text-xsm text-green-500 block"}>mm</span>
                      </th>
                      <th className={"text-left bg-gray-100 py-2 pr-4"}>
                        <span className={"uppercase font-bold block text-sm"}>Délka</span>
                        <span className={"text-xsm text-green-500 block"}>mm</span>
                      </th>
                      <th className={"text-left bg-gray-100 py-2 pr-4"}>
                        <span className={"uppercase font-bold block text-sm"}>Zrno</span>
                        <span className={"text-xsm text-green-500 block"}>FEPA</span>
                      </th>
                      <th className={"text-left bg-gray-100 py-2 pr-4"}>
                        <span className={"uppercase font-bold block text-sm"}>Počet</span>
                        <span className={"text-xsm text-green-500 block"}>ks</span>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td className={"text-center border-b border-gray-400"}>1</td>
                      <td className={"py-2 border-b border-gray-400"}><input type="text" className={"px-3 py-2.5 border border-blue-basic rounded-lg w-20"}/></td>
                      <td className={"py-2 border-b border-gray-400"}><input type="text" className={"px-3 py-2.5 border border-blue-basic rounded-lg w-20"}/></td>
                      <td className={"py-2 border-b border-gray-400"}><input type="text" className={"px-3 py-2.5 border border-blue-basic rounded-lg w-20"}/></td>
                      <td className={"py-2 border-b border-gray-400"}><input type="text" className={"px-3 py-2.5 border border-blue-basic rounded-lg w-40"}/></td>
                    </tr>
                    <tr>
                      <td colSpan={5} className={"py-4 px-5"}>
                        <span className={"text-red-light uppercase font-bold text-sm"}>+ Přidat další variantu</span>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                  <div className="flex-between-center px-4 pb-4 mt-2">
                    <ActionButton link={"#"}>Jít do košíku</ActionButton>
                    <ActionButton link={"#"} icon={<DoubleChevronIcon className={"[&>path]:fill-red-light"}/>}>Dokončit poptávku</ActionButton>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </article>
      <Footer/>
    </>
  )
}
