import Header from "../../_theme/Header/Header";
import Footer from "../../_theme/Footer/Footer";
import Image from "next/image";
import React from "react";
import BButton from "../../_theme/Buttons/BButton";

export default function ArchiveNew() {
  return (
    <main>
      <div className="CategorySection">
        <div className="CategoryTitle w-full bg-gradient-to-t from-gray-100/50 to-white py-3 border-b border-black">
          <div className="container-fluid">
            <h1 className={"text-lg3 font-medium text-blue-basic"}>Keramika</h1>
          </div>
        </div>
        <div className="grid grid-cols-5 grid-test">
          {[...Array(8)].map((e, i) => <ProductBox key={i}/>)}
        </div>
      </div>
    </main>
  )
}


export function ProductBox() {
  return (
    <article className="bg-white grid__item">
      <div className="py-6 px-6 relative">
        <div className="absolute right-6 top-6 text-[11px] bg-blue-basic rounded-md uppercase py-0.5 px-2 text-white font-semibold">Nejlepší nabídka</div>
        <div className="flex-center">
          <Image alt={""} src={"/img/product-AK890Y-big.jpg"} width={290} height={290} className={"w-10/12"}/>
        </div>
        <h1 className={"text-blue-basic font-bold text-lg mt-3"}>AK890Y</h1>
        <p className={"text-sm mt-2"}>Brusivo XK733X je špičkou v obrábění hliníku a neželezných kovů. Přídavná vrstva VSM ALU-X a polootevřený rozptyl redukují ulpívání třísek a optimalizují odvádění třísek. </p>
        <div className={"mt-2"}>
          <h2 className="text-green-500 font-semibold text-xsm">Nejlepší k broušení:</h2>
          <p className={"text-sm w-full"}>hliník, mosaz, dřevo</p>
        </div>
        <BButton buttonType={"normal"} size={"big"} link={"#"} className={"w-full text-center mt-3"}>Zobrazit detail</BButton>
      </div>
    </article>
  )
}
