import {Button} from '../_components/Button'
import {Gutter} from '../_components/Gutter'
import {VerticalPadding} from '../_components/VerticalPadding'
import {centerCrop} from "react-image-crop";
import Link from "next/link";

export default function NotFound() {
  return (
    <div className={"flex-center min-h-96"}>
      <div className="text-center flex-wrap">
        <h1 className={"text-lg4 font-semibold text-blue-basic mb-4"}>Stránka nenalezena</h1>
        <Link href={"/"}>Zpět na homepage</Link>
      </div>
    </div>
  )
}
