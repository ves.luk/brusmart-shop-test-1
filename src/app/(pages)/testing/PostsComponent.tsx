'use client'; // This is a client-side component

import { useState, useEffect } from 'react';

const PostsComponent = ({ initialPosts }) => {
  const [posts, setPosts] = useState(initialPosts);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    const fetchPosts = async () => {
      const res = await fetch(`/api/search?title=${searchTerm}`);
      const data = await res.json();
      setPosts(data.docs);
    };

    if (searchTerm) {
      fetchPosts();
    } else {
      setPosts(initialPosts);
    }
  }, [searchTerm, initialPosts]);

  return (
    <div>
      <h1>All Posts</h1>
      <input
        type="text"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
        placeholder="Search by title"
      />
      <ul>
        {posts.map((post) => (
          <li key={post.id}>{post.title}</li>
        ))}
      </ul>
    </div>
  );
};

export default PostsComponent;
