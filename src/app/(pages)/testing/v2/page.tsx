'use client'

import React, { Fragment, useCallback, useEffect, useRef, useState } from 'react'
import qs from 'qs'

import {Card} from "../../../_components/Card";
import {Gutter} from "../../../_components/Gutter";
import type {Product} from "../../../../payload/payload-types";
import type {ArchiveBlockProps} from "../../../_blocks/ArchiveBlock/types";
import {use} from "i18next";

type Result = {
  docs: (Product | string)[]
  hasNextPage: boolean
  hasPrevPage: boolean
  nextPage: number
  page: number
  prevPage: number
  totalDocs: number
  totalPages: number
}

export type Props = {
  categories?: ArchiveBlockProps['categories']
  className?: string
  limit?: number
  onResultChange?: (result: Result) => void // eslint-disable-line no-unused-vars
  populateBy?: 'collection' | 'selection'
  populatedDocs?: ArchiveBlockProps['populatedDocs']
  populatedDocsTotal?: ArchiveBlockProps['populatedDocsTotal']
  relationTo?: 'products'
  selectedDocs?: ArchiveBlockProps['selectedDocs']
  showPageRange?: boolean
  sort?: string
}


export default function TestingV2(props) {
  const {
    categories: catsFromProps,
    className,
    limit = 10,
    onResultChange,
    populateBy,
    populatedDocs,
    populatedDocsTotal,
    relationTo = "products",
    selectedDocs,
    showPageRange,
    sort = '-createdAt',
  } = props

  const [results, setResults] = useState<Result>({
    docs: (populateBy === 'collection'
        ? populatedDocs
        : populateBy === 'selection'
          ? selectedDocs
          : []
    )?.map(doc => doc.value),
    hasNextPage: false,
    hasPrevPage: false,
    nextPage: 1,
    page: 1,
    prevPage: 1,
    totalDocs: typeof populatedDocsTotal === 'number' ? populatedDocsTotal : 0,
    totalPages: 1,
  })

  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState<string | undefined>(undefined)
  const scrollRef = useRef<HTMLDivElement>(null)
  const hasHydrated = useRef(false)
  const isRequesting = useRef(false)
  const [page, setPage] = useState(1)

  const categories = (catsFromProps || [])
    .map(cat => (typeof cat === 'object' ? cat?.id : cat))
    .join(',')

  const scrollToRef = useCallback(() => {
    const { current } = scrollRef
    if (current) {
      // current.scrollIntoView({
      //   behavior: 'smooth',
      // })
    }
  }, [])

  useEffect(() => {
    makeRequest();
  }, []);

  useEffect(() => {
    if (!isLoading && typeof results.page !== 'undefined') {
      // scrollToRef()
    }
  }, [isLoading, scrollToRef, results])


  const searchQuery = qs.stringify(
    {
      depth: 1,
      limit,
      page,
      sort,
      where: {

      },
    },
    { encode: false },
  )
  const makeRequest = async () => {
    try {
      const req = await fetch(
        `${process.env.NEXT_PUBLIC_SERVER_URL}/api/${relationTo}?${searchQuery}`,
      )

      const json = await req.json()
      console.log(json)


      const { docs } = json as { docs: Product[] }

      if (docs && Array.isArray(docs)) {
        setResults(json)
        setIsLoading(false)
        if (typeof onResultChange === 'function') {
          onResultChange(json)
        }
      }
    } catch (err) {
      console.warn(err) // eslint-disable-line no-console
      setIsLoading(false)
      setError(`Unable to load "${relationTo} archive" data at this time.`)
    }

    isRequesting.current = false
    hasHydrated.current = true
  }

  const [searchTerm, setSearchTerm] = useState('');
  const [selectedCategories, setSelectedCategories] = useState([]);

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
  };



  const handleCategoryChange = (event) => {
    const category = event.target.value;
    setSelectedCategories((prevCategories) =>
      prevCategories.includes(category)
        ? prevCategories.filter((cat) => cat !== category)
        : [...prevCategories, category]
    );
  };

  const filteredProducts = results.docs.filter((product) => {
    const matchesSearchTerm = product.title.toLowerCase().includes(searchTerm.toLowerCase());
    const productCategories = product.categories.map(cat => cat.title);
    const matchesCategory = selectedCategories.length === 0 || selectedCategories.some(category => productCategories.includes(category));
    return matchesSearchTerm && matchesCategory;
  });

  console.log(filteredProducts);



  return (
    <div>
      <button onClick={makeRequest}>dfsdsdsf</button>

      <div>
        <h1>Product List</h1>
        <input
          type="text"
          placeholder="Search products"
          value={searchTerm}
          onChange={handleSearch}
        />
        <div>
          <label>
            <input
              type="checkbox"
              value="Brusný kotouč"
              checked={selectedCategories.includes('Brusný kotouč')}
              onChange={handleCategoryChange}
            />
            Brusný kotouč
          </label>
          <label>
            <input
              type="checkbox"
              value="Brusný pás"
              checked={selectedCategories.includes('Brusný pás')}
              onChange={handleCategoryChange}
            />
            Brusný pás
          </label>
          {/* Add more checkboxes as needed */}
        </div>
        <ul>
          {filteredProducts.map((product) => (
            <li key={product.id}>{product.title}</li>
          ))}
        </ul>
      </div>


      <div ref={scrollRef}/>
      {!isLoading && error && <Gutter>{error}</Gutter>}
      {/*{results.docs?.map((result, index) => {
        if (typeof result === 'object' && result !== null) {
          return (
            <div key={index}>
              <Card doc={result} relationTo={relationTo} showCategories/>
            </div>
          )
        }

        return null
      })}*/}
    </div>
  )
}
