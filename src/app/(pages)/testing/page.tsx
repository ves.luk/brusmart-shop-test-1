import payload from "payload";
import PostsComponent from "./PostsComponent";

export const dynamic = 'force-dynamic'; // Optional: if you want the page to be dynamic

async function fetchPosts() {
  const result = await payload.find({
    collection: 'pages', // required
    depth: 2,
    page: 1,
    limit: 10,
    pagination: false, // If you want to disable pagination count, etc.
    where: {}, // Initially fetch all posts
    sort: '-title',
    locale: 'en',
    fallbackLocale: false,
    user: null, // replace with your actual user if needed
    overrideAccess: false,
    showHiddenFields: true,
  });
  return result.docs;
}

export default async function PostsPage() {
  const posts = await fetchPosts();

  return <PostsComponent initialPosts={posts} />;
}
