import React from 'react'
import {Metadata} from 'next'
import {draftMode} from 'next/headers'
import {notFound} from 'next/navigation'

import {Product, Product as ProductType} from '../../../../payload/payload-types'
import {fetchDoc} from '../../../_api/fetchDoc'
import {fetchDocs} from '../../../_api/fetchDocs'
import {Blocks} from '../../../_components/Blocks'
import {PaywallBlocks} from '../../../_components/PaywallBlocks'
import {ProductHero} from '../../../_heros/Product'
import {generateMeta} from '../../../_utilities/generateMeta'
import Image from "next/image";
import MaterialButton from "../../../_theme/Buttons/MaterialButton";
import Link from "next/link";
import PdfIcon from "../../../../../public/icons/pdf.svg";
import ActionButton from "../../../_theme/Buttons/ActionButton";
import DoubleChevronIcon from "../../../../../public/icons/double-chevron.svg";
import RichText from "../../../_theme/Functional/RichText";
import classes from "../../../_heros/Product/index.module.scss";
import {AddToCartButton} from "../../../_components/AddToCartButton";
import {renameMaterials} from "../../../_theme/Functional/RenameMaterials";
import {renameTechnicalInformation} from "../../../_theme/Functional/RenameTechnicalInformation";
import {renameMachine} from "../../../_theme/Functional/RenameMachine";

// Force this page to be dynamic so that Next.js does not cache it
// See the note in '../../../[slug]/page.tsx' about this
export const dynamic = 'force-dynamic'

export default async function Product({params: {slug}}) {
  const {isEnabled: isDraftMode} = draftMode()

  let product: Product | null = null

  try {
    product = await fetchDoc<Product>({
      collection: 'products',
      slug,
      draft: isDraftMode,
    })
  } catch (error) {
    console.error(error) // eslint-disable-line no-console
  }

  if (!product) {
    notFound()
  }

  const {layout, relatedProducts} = product

  const categoryName = product.categories[0].id == 1 && "Brusný pás";
  return (

    <React.Fragment>
      <article>
        <div className="container">
          <div className="grid grid-cols-12 gap-10">
            <div className="col-span-8">
              {/* --------- MAIN CONTENT*/}

              {/* HERO */}
              <div className="flex-between-start mt-10 gap-10">
                <div className={"flex-1"}>
                  <h1 className={"text-xl text-blue-basic font-light leading-[3rem]"}><span className={"text-red-light text-lg2"}>{categoryName}</span><br/>{product.title}</h1>
                  <p className={"text-base mt-4"}>{product.long_description}</p>
                </div>
                <div className="flex-center h-full">
                  <Image alt={""} src={product.main_image?.url} width={260} height={260} className={"mt-6 object-contain mr-10"}/>
                </div>
              </div>
              {/* HERO INFO - MATERIALS */}
              <div className="space-y-2.5">
                <div>
                  <span className="font-semibold text-green-500 text-sm">Technologie zrna:</span>
                  <p className={"font-semibold text-md2 uppercase"}>{product.technology.title}</p>
                </div>
                <div>
                  <span className="font-semibold text-green-500 text-sm">Vyvinuto pro:</span>
                  <ul className={"flex-start-start flex-wrap gap-3 mt-1"}>
                    {renameMaterials(product.for_material_fit).map(material => (
                      <li><MaterialButton link={"#"} icon={<img src={`/materials/${material.name}.png`} alt=""/>}>{material.label}</MaterialButton></li>
                    ))}
                  </ul>
                </div>
                <div>
                  <span className="font-semibold text-green-500 text-sm">Vhodné také pro:</span>
                  <ul className={"flex-start-start flex-wrap gap-3 mt-1"}>
                    {renameMaterials(product.for_material_also).map(material => (
                      <li><MaterialButton link={"#"} icon={<img src={`/materials/${material.name}.png`} alt=""/>}>{material.label}</MaterialButton></li>
                    ))}
                  </ul>
                </div>
              </div>

              {/* DIVIDER */}
              <div className="w-full h-px origin-top-left bg-gradient-to-r from-white via-gray-400 to-white my-6"/>

              {/* BENEFITS */}
              <div>
                <h2 className={"text-lg2 font-light"}>Benefity {product.title}</h2>
                <div className={"mt-2"}>
                  <ul className={"text-base font-normal space-y-1.5 benefits-list"}>
                    {product.benefits.map((benefit, i) =>
                      <li key={i}>{benefit.text}</li>
                    )}
                  </ul>
                </div>
              </div>

              {/* DIVIDER */}
              <div className="w-full h-px origin-top-left bg-gradient-to-r from-white via-gray-400 to-white my-6"/>

              {/* MORE ABOUT PRODUCT */}
              <div>
                <h2 className={"text-lg2 font-light"}>Více o produktu</h2>
                <p className={"mt-2 text-base"}>Plus v brusném výkonu
                  Praxe ukázala, že brusiva ACTIROX mají vyšší brusný výkon než srovnatelná konvenční brusiva – s konzistentním výsledným povrchem. S konkrétní zrnitostí dosáhnete většího úběru, než byste očekávali, ale povrch není drsnější. Skutečné plus pro váš proces broušení.</p>
                <RichText content={product.more_about_product_description}/>
              </div>

              {/* TECHNICAL INFO - TABLE */}
              <div className={"mt-8"}>
                <h3 className={"text-md3 font-light"}>Technické údaje</h3>
                <ul className={"divide-y divide-green-300 border-y border-green-300 mt-2 max-w-sm"}>
                  {renameTechnicalInformation(product.technical_information).map(info => (
                    <li className={"flex-between-center py-2 px-3 flex-wrap"}><span className={"text-green-500 font-medium text-base"}>{info.label}:</span> <span className={"text-base"}>{info.text}</span></li>
                  ))}
                  <li className={"flex-between-center py-2 px-3 flex-wrap"}><span className={"text-green-500 font-medium text-base"}>Vhodné pro stroje:</span>
                    <span className={"text-base"}>
                    {renameMachine(product.for_machine).map((machine, i) =>
                      <span>{i >= 1 && "|"} {machine.label}</span>
                    )}
                    </span>
                  </li>
                </ul>
              </div>

              {/* DOWNLOAD FILES - INFORMATION MATERIALS */}
              <div className={"mt-8"}>
                <h3 className={"text-md3 font-light"}>Informační material</h3>
                <div className={"space-y-1 mt-2"}>
                  {product.files_for_download.map((file, i) =>
                    <Link href={file.file.url} download={true} className={"flex-start-center gap-2"}>
                      <div className="bg-gray-100 rounded-lg py-1 px-1 inline-block"><PdfIcon className={"[&>path]:fill-red-light w-5 h-5"}/></div>
                      {file.name}</Link>
                  )}
                </div>
              </div>
            </div>
            <div className="col-span-4">
              <div className="border border-gray-400 rounded-lg mt-12 sticky top-12">
                <div className="flex-between-center py-3 px-5">
                  <span className={"text-md2"}>Vytvořit pás na míru</span>
                  {/*<AddToCartButton product={product} className={classes.addToCartButton}/>*/}
                  <span className={"text-green-500"}>Uloženo</span>
                </div>
                <div>
                  <table className={"w-full"}>
                    <thead>
                    <tr>
                      <th className={"text-center bg-gray-100 py-2 px-5"}>#</th>
                      <th className={"text-left bg-gray-100 py-2 pr-4"}>
                        <span className={"uppercase font-bold block text-sm"}>Šířka</span>
                        <span className={"text-xsm text-green-500 block"}>mm</span>
                      </th>
                      <th className={"text-left bg-gray-100 py-2 pr-4"}>
                        <span className={"uppercase font-bold block text-sm"}>Délka</span>
                        <span className={"text-xsm text-green-500 block"}>mm</span>
                      </th>
                      <th className={"text-left bg-gray-100 py-2 pr-4"}>
                        <span className={"uppercase font-bold block text-sm"}>Zrno</span>
                        <span className={"text-xsm text-green-500 block"}>FEPA</span>
                      </th>
                      <th className={"text-left bg-gray-100 py-2 pr-4"}>
                        <span className={"uppercase font-bold block text-sm"}>Počet</span>
                        <span className={"text-xsm text-green-500 block"}>ks</span>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td className={"text-center border-b border-gray-400 font-bold"}>1</td>
                      <td className={"py-2 border-b border-gray-400"}><input type="number" className={"px-3 py-2.5 border border-blue-basic rounded-lg w-16"}/></td>
                      <td className={"py-2 border-b border-gray-400"}><input type="number" className={"px-3 py-2.5 border border-blue-basic rounded-lg w-16"}/></td>
                      <td className={"py-2 border-b border-gray-400"}><input type="number" className={"px-3 py-2.5 border border-blue-basic rounded-lg w-16"}/></td>
                      <td className={"py-2 border-b border-gray-400"}><input type="number" className={"px-3 py-2.5 border border-blue-basic rounded-lg w-20"}/></td>
                    </tr>
                    <tr>
                      <td colSpan={5} className={"py-4 px-5"}>
                        <span className={"text-red-light uppercase font-bold text-sm"}>+ Přidat další variantu</span>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                  <div className="flex-between-center px-4 pb-4 mt-2 [&>div]:w-full">
                    <ActionButton link={"#"} appearance={"primary"} icon={<DoubleChevronIcon className={"[&>path]:fill-red-light"}/>}>Odeslat poptávku</ActionButton>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </article>

      {/*<ProductHero product={product} />*/}
      {/*<Blocks blocks={layout} />*/}
      {/*{product?.enablePaywall && <PaywallBlocks productSlug={slug as string} disableTopPadding />}
      <Blocks
        disableTopPadding
        blocks={[
          {
            blockType: 'relatedProducts',
            blockName: 'Related Product',
            relationTo: 'products',
            introContent: [
              {
                type: 'h4',
                children: [
                  {
                    text: 'Related Products',
                  },
                ],
              },
              {
                type: 'p',
                children: [
                  {
                    text: 'The products displayed here are individually selected for this page. Admins can select any number of related products to display here and the layout will adjust accordingly. Alternatively, you could swap this out for the "Archive" block to automatically populate products by category complete with pagination. To manage related posts, ',
                  },
                  {
                    type: 'link',
                    url: `/admin/collections/products/${product.id}`,
                    children: [
                      {
                        text: 'navigate to the admin dashboard',
                      },
                    ],
                  },
                  {
                    text: '.',
                  },
                ],
              },
            ],
            docs: relatedProducts,
          },
        ]}
      />*/}
    </React.Fragment>
  )
}

export async function generateStaticParams() {
  try {
    const products = await fetchDocs<ProductType>('products')
    return products?.map(({slug}) => slug)
  } catch (error) {
    return []
  }
}

export async function generateMetadata({params: {slug}}): Promise<Metadata> {
  const {isEnabled: isDraftMode} = draftMode()

  let product: Product | null = null

  try {
    product = await fetchDoc<Product>({
      collection: 'products',
      slug,
      draft: isDraftMode,
    })
  } catch (error) {
  }

  return generateMeta({doc: product})
}

