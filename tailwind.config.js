/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      /*fontFamily: {
        primary: ['Montserrat', 'sans-serif'],
      },*/
      colors: {
        red: {
          'basic': '#C62E35',
          'light': '#E83941',
        },
        blue: {
          'basic': '#052943',
        },
        green: {
          300: '#C6D7DB',
          500: '#5E767F',
        },
        gray: {
          100: '#F1F6F7',
          300: '#EDF2F6',
          400: '#CBD9E4',
          500: '#D6D6D6',
        },
      },
      fontSize: {
        '3xl': '80px',
        '2xl': '61px',
        'xl': ['55px', '62px'],
        'lg4': '40px',
        'lg3': '35px',
        'lg2': '30px',
        'lg': '25px',
        'md3': '20px',
        'md2': '17px',
        'md': '15px',
        'base': '14px',
        'sm': '13px',
        'xsm': '12px',
      },
      container: {
        center: true,
        padding: {
          DEFAULT: '20px',
          sm: '20px',
          lg: '40px',
          xl: '50px',
          '2xl': '50px',
        },
        screens: {
          sm: '640px',
          md: '768px',
          lg: '1024px',
          xl: '1280px',
          '2xl:': '1512px',
        },
      },
    },
  },
  plugins: [],
}

